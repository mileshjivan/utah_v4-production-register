/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Production_Register_V5_2_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author syotsi
 */
public class FR1_LinkBackToRecord_PageObjects extends BaseClass {

   
    public static String office_URL() 
    {
       return "https://www.office.com";
    }

    public static String office_signin()
    {
         return ".//a[contains(text(),'Sign in')]";
    }

    public static String office_email_id()
    {
       return ".//input[@type='email']";
    }

    public static String email_next_btn()
    {
       return ".//input[@value='Next']";
    }
    
    public static String office_password()
    {
       return "//input[@type='password']";
    }

    public static String office_signin_btn()
    {
        return "//input[@value='Sign in']";
    }

    public static String office_No_btn()
    {
        return "//input[@value='No']";
    }

    public static String outlook_icon()
    {
       return ".//div[@id='ShellMail_link_text']";
    }

    public static String other_folder()
    {
       return "//span[text()='Other']";
    }
    
    public static String nonProduction_folder(){
        return "//span[text()='Non Production']";
    }

    
    public static String email_notification(String text)
    {
       return "(//span[contains(text(),'"+text+"')])[1]";
    }


    public static String linkBackToRecord_Link()
    {
       return ".//a[@data-auth='NotApplicable']";
    }

    public static String Username()
    {
       return "//input[@id='txtUsername']";
    }

    public static String Password()
    {
        return "//input[@id='txtPassword']";
    }

    public static String LoginBtn() 
    {
         return "//div[@id='btnLoginSubmit']";
    }

    
}
